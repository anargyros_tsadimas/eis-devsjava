package eis;

import simView.*;
import genDevs.modeling.*;

/**
 *
 * @author gdkapos
 */
public class Client extends ViewableAtomic {

    protected double interval;

    public Client() {
        this("client", 200);
    }

    public Client(String name, double interval) {
        super(name);
        this.interval = interval;
        addInport("in");
        addOutport("out");
    }

    public void initialize() {
        phase = "passive";
        sigma = this.interval;
        super.initialize();
    }

    public void deltext(double e, message x) {
        Continue(e);
        if (this.somethingOnPort(x, "in")) {
            Packet packet = (Packet) getEntityOnPort(x, "in");
            if (!packet.sendRespond && packet.sourceNodeId.equals(this.getName())) {
                System.out.println("Service call completed");
            }
        }
    }

    /* Ειμαστε εντάξει με το currentPacket αν καλείται πρώτα η out() και μετά η
    deltint() */
    public message out() {
        Packet packet = new Packet(this.getName(), null, null, "node", "module", "service", 2000, true);
        message m = new message();
        m.add(makeContent("out", packet));
        return m;
    }

    public void deltint() {
        this.holdIn("passive", interval);
    }
}