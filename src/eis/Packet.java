package eis;

import GenCol.entity;

/**
 *
 * @author gdkapos
 */
public class Packet extends entity {
    public String sourceNodeId;
    public String sourceModuleId;
    public String sourceServiceId;
    public String targetNodeId;
    public String targetModuleId;
    public String targetServiceId;
    public long size;
    public boolean sendRespond; // true->send, false->respond

    public Packet (String snid, String smid, String ssid, String tnid,
            String tmid, String tsid, long size, boolean sr) {
        this.sourceNodeId = snid;
        this.sourceModuleId = smid;
        this.sourceServiceId = ssid;
        this.targetNodeId = tnid;
        this.targetModuleId = tmid;
        this.targetServiceId = tsid;
        this.size = size;
        this.sendRespond = sr;
    }
}
