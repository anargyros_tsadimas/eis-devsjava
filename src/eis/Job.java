package eis;

import GenCol.entity;

/**
 *
 * @author gdkapos
 */
public class Job extends entity {
    public String serviceId;
    public long calcs;

    public Job (String sid, long calcs) {
        this.serviceId = sid;
        this.calcs = calcs;
    }
}
