package eis;

import simView.*;
import genDevs.modeling.*;
import java.util.ArrayList;

/**
 *
 * @author gdkapos
 */
public class Service extends ViewableAtomic {

    protected String nodeId, moduleId;
    protected ArrayList<Packet> queue; // Ουρά με πακέτα request
    protected Packet currentPacket;
    protected Job job;
    protected Block block;
    protected Packet packet;

    public Service() {
        this("service", "node", "module",
                new Job("service", 50000),
                new Block("service", 10000),
                new Packet("node", "module", "service", "node", "module", "service2", 1500, true));
    }

    public Service(String name, String nodeId, String moduleId, Job job, Block block, Packet packet) {
        super(name);
        this.nodeId = nodeId;
        this.moduleId = moduleId;
        this.job = job;
        this.job.serviceId = this.getName();
        this.block = block;
        this.block.serviceId = this.getName();
        this.packet = packet;
        this.queue = new ArrayList<Packet>();
        addInport("processorResp");
        addOutport("processorReq");
        addInport("storageResp");
        addOutport("storageReq");
        addInport("networkIn");
        addOutport("networkOut");
    }

    public void initialize() {
        phase = "passive";
        sigma = INFINITY;
        super.initialize();
    }

    public void deltext(double e, message x) {
        Continue(e);
        if (this.somethingOnPort(x, "networkIn")) {
            Packet packet = (Packet) getEntityOnPort(x, "networkIn");
            if (packet.sendRespond && packet.targetServiceId.equals(this.getName())) {
                if (phaseIs("passive")) {
                    currentPacket = packet;
                    if (this.block != null) {
                        this.holdIn("accessingStorage", 0);
                    } else if (this.packet != null) {
                        this.holdIn("accessingNetwork", 0);
                    } else if (this.job != null) {
                        this.holdIn("processing", 0);
                    } else {
                        this.holdIn("responding", 0);
                    }
                } else {
                    this.queue.add(packet);
                }
            } else if (!packet.sendRespond && packet.sourceServiceId.equals(this.getName())
                    && phaseIs("waitingNetwork")) {
                if (this.job != null) {
                    this.holdIn("processing", 0);
                } else {
                    this.holdIn("responding", 0);
                }
            }
        }
        if (this.somethingOnPort(x, "storageResp")) {
            Block block = (Block) getEntityOnPort(x, "storageResp");
            if (block.serviceId.equals(this.getName())) {
                if (this.packet != null) {
                    this.holdIn("accessingNetwork", 0);
                } else if (this.job != null) {
                    this.holdIn("processing", 0);
                } else {
                    this.holdIn("responding", 0);
                }
            }
        }
        if (this.somethingOnPort(x, "processorResp")) {
            Job job = (Job) getEntityOnPort(x, "processorResp");
            if (job.serviceId.equals(this.getName())) {
                this.holdIn("responding", 0);
            }
        }
    }

    /* Ειμαστε εντάξει με το currentPacket αν καλείται πρώτα η out() και μετά η
    deltint() */
    public message out() {
        message m = new message();
        if (phaseIs("accessingStorage")) {
            m.add(makeContent("storageReq", this.block));
        }
        if (phaseIs("accessingNetwork")) {
            m.add(makeContent("networkOut", this.packet));
        }
        if (phaseIs("processing")) {
            m.add(makeContent("processorReq", this.job));
        }
        if (phaseIs("responding")) {
            this.currentPacket.sendRespond = false;
            m.add(makeContent("networkOut", this.currentPacket));
        }
        return m;
    }

    public void deltint() {
        if (phaseIs("accessingStorage")) {
            passivateIn("waitingStorage");
        }
        if (phaseIs("accessingNetwork")) {
            passivateIn("waitingNetwork");
        }
        if (phaseIs("processing")) {
            passivateIn("waitingProcessor");
        }
        if (phaseIs("responding")) {
            if (this.queue.size() > 0) {
                Packet packet = this.queue.get(0);
                this.queue.remove(0);
                currentPacket = packet;
                if (this.block != null) {
                    this.holdIn("accessingStorage", 0);
                } else if (this.packet != null) {
                    this.holdIn("accessingNetwork", 0);
                } else if (this.job != null) {
                    this.holdIn("processing", 0);
                } else {
                    this.holdIn("responding", 0);
                }
            } else {
                passivate();
            }
        }
    }
}
