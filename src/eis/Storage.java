package eis;

import simView.*;
import genDevs.modeling.*;
import java.util.ArrayList;

/**
 *
 * @author gdkapos
 */
public class Storage extends ViewableAtomic {

    protected double throughput, idleTime, busyTime;
    protected ArrayList<Block> queue;
    protected Block currentBlock;

    public Storage() {
        this("storage", 2000);
    }

    public Storage(String name, double throughput) {
        super(name);
        this.throughput = throughput;
        this.queue = new ArrayList<Block>();
        this.idleTime = 0;
        this.busyTime = 0;
        addInport("in");
        addOutport("out");

        //addTestInput("in", new Job("s1",1000000));
    }

    public void initialize() {
        phase = "passive";
        sigma = INFINITY;
        //phase = "busy";
        //sigma = 15;
        //this.queue.add(new Job("s1",1000000));
        //this.queue.add(new Job("s2",2000000));
        //this.queue.add(new Job("s3",3000000));
        super.initialize();
    }

    public void deltext(double e, message x) {
        Continue(e);
        Block block = (Block) getEntityOnPort(x, "in");
        busyTime += block.size / this.throughput;
        if (phaseIs("passive")) {
            idleTime += e;
            currentBlock = block;
            this.holdIn("busy", block.size / this.throughput);
        } else {
            this.queue.add(block);
        }
    }

    /* Ειμαστε εντάξει με το currentBlock αν καλείται πρώτα η out() και μετά η
       deltint() */
    public message out() {
        message m = new message();
        if (phaseIs("busy")) {
            m.add(makeContent("out", currentBlock));
        }
        return m;
    }

    public void deltint() {
        if (phaseIs("busy")) {
            if (this.queue.size() > 0) {
                Block block = this.queue.get(0);
                this.queue.remove(0);
                this.holdIn("busy", block.size / this.throughput);
                currentBlock = block;
            } else {
                passivate();
            }
        }
    }
}