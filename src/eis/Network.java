package eis;

import simView.*;
import genDevs.modeling.*;
import java.util.ArrayList;

/**
 *
 * @author gdkapos
 */
public class Network extends ViewableAtomic {

    protected double throughput, idleTime, busyTime;
    protected ArrayList<Packet> queue;
    protected ArrayList<String> lowerNodes;
    protected Packet currentPacket;

    public Network() {
        this("network", 2000, null);
        this.lowerNodes = new ArrayList<String>();
        this.lowerNodes.add("node");
    }

    public Network(String name, double throughput, ArrayList<String> lowerNodes) {
        super(name);
        this.throughput = throughput;
        this.queue = new ArrayList<Packet>();
        this.lowerNodes = lowerNodes;
        this.idleTime = 0;
        this.busyTime = 0;
        addInport("upperIn");
        addOutport("upperOut");
        addInport("lowerIn");
        addOutport("lowerOut");
    }

    public void initialize() {
        phase = "passive";
        sigma = INFINITY;
        super.initialize();
    }

    public void deltext(double e, message x) {
        Continue(e);
        if (this.somethingOnPort(x, "upperIn")) {
            Packet packet = (Packet) getEntityOnPort(x, "upperIn");
            if (packet.sendRespond && this.lowerNodes.contains(packet.targetNodeId)
                    || !packet.sendRespond && this.lowerNodes.contains(packet.sourceNodeId)) {
                busyTime += packet.size / this.throughput;
                if (phaseIs("passive")) {
                    idleTime += e;
                    currentPacket = packet;
                    this.holdIn("busy", packet.size / this.throughput);
                } else {
                    this.queue.add(packet);
                }
            }
        }
        if (this.somethingOnPort(x, "lowerIn")) {
            Packet packet = (Packet) getEntityOnPort(x, "lowerIn");
                busyTime += packet.size / this.throughput;
                if (phaseIs("passive")) {
                    idleTime += e;
                    currentPacket = packet;
                    this.holdIn("busy", packet.size / this.throughput);
                } else {
                    this.queue.add(packet);
                }
        }
    }

    /* Ειμαστε εντάξει με το currentPacket αν καλείται πρώτα η out() και μετά η
    deltint() */
    public message out() {
        message m = new message();
        if (phaseIs("busy")) {
            if (currentPacket.sendRespond && this.lowerNodes.contains(currentPacket.targetNodeId)
                    || !currentPacket.sendRespond && this.lowerNodes.contains(currentPacket.sourceNodeId)) {
                m.add(makeContent("lowerOut", currentPacket));
            } else {
                m.add(makeContent("upperOut", currentPacket));
            }
        }
        return m;
    }

    public void deltint() {
        if (phaseIs("busy")) {
            if (this.queue.size() > 0) {
                Packet packet = this.queue.get(0);
                this.queue.remove(0);
                this.holdIn("busy", packet.size / this.throughput);
                currentPacket = packet;
            } else {
                passivate();
            }
        }
    }
}