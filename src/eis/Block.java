package eis;

import GenCol.entity;

/**
 *
 * @author gdkapos
 */
public class Block extends entity {
    public String serviceId;
    public long size;

    public Block (String sid, long size) {
        this.serviceId = sid;
        this.size = size;
    }
}
