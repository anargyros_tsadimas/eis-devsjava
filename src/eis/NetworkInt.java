package eis;

import simView.*;
import genDevs.modeling.*;
import java.util.ArrayList;

/**
 *
 * @author gdkapos
 */
public class NetworkInt extends ViewableAtomic {

    protected double throughput, idleTime, busyTime;
    protected String nodeId;
    protected ArrayList<Packet> queue;
    protected Packet currentPacket;

    public NetworkInt() {
        this("networkInt", "node", 2000);
    }

    public NetworkInt(String name, String nodeId, double throughput) {
        super(name);
        this.nodeId = nodeId;
        this.throughput = throughput;
        this.queue = new ArrayList<Packet>();
        this.idleTime = 0;
        this.busyTime = 0;
        addInport("extIn");
        addOutport("extOut");
        addInport("intIn");
        addOutport("intOut");
    }

    public void initialize() {
        phase = "passive";
        sigma = INFINITY;
        super.initialize();
    }

    public void deltext(double e, message x) {
        Continue(e);
        if (this.somethingOnPort(x, "extIn")) {
            Packet packet = (Packet) getEntityOnPort(x, "extIn");
            if (packet.sendRespond && packet.targetNodeId.equals(this.nodeId)
                    || !packet.sendRespond && packet.sourceNodeId.equals(this.nodeId)) {
                busyTime += packet.size / this.throughput;
                if (phaseIs("passive")) {
                    idleTime += e;
                    currentPacket = packet;
                    this.holdIn("busy", packet.size / this.throughput);
                } else {
                    this.queue.add(packet);
                }
            }
        }
        if (this.somethingOnPort(x, "intIn")) {
            Packet packet = (Packet) getEntityOnPort(x, "intIn");
            busyTime += packet.size / this.throughput;
            if (phaseIs("passive")) {
                idleTime += e;
                currentPacket = packet;
                this.holdIn("busy", packet.size / this.throughput);
            } else {
                this.queue.add(packet);
            }
        }
    }

    /* Ειμαστε εντάξει με το currentPacket αν καλείται πρώτα η out() και μετά η
    deltint() */
    public message out() {
        message m = new message();
        if (phaseIs("busy")) {
            if (currentPacket.sendRespond && !currentPacket.targetNodeId.equals(this.nodeId)
                    || !currentPacket.sendRespond && !currentPacket.sourceNodeId.equals(this.nodeId)) {
                m.add(makeContent("extOut", currentPacket));
            } else if (currentPacket.sendRespond && currentPacket.targetNodeId.equals(this.nodeId)
                    || !currentPacket.sendRespond && currentPacket.sourceNodeId.equals(this.nodeId)) {
                m.add(makeContent("intOut", currentPacket));
            }
        }
        return m;
    }

    public void deltint() {
        if (phaseIs("busy")) {
            if (this.queue.size() > 0) {
                Packet packet = this.queue.get(0);
                this.queue.remove(0);
                this.holdIn("busy", packet.size / this.throughput);
                currentPacket = packet;
            } else {
                passivate();
            }
        }
    }
}
