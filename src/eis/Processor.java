package eis;

import simView.*;
import genDevs.modeling.*;
import java.util.ArrayList;

/**
 *
 * @author gdkapos
 */
public class Processor extends ViewableAtomic {

    protected double power, idleTime, busyTime;
    protected int cores;
    protected ArrayList<Job> queue;
    protected Job currentJob;

    public Processor() {
        this("processor", 2000, 1);
    }

    public Processor(String name, double power, int cores) {
        super(name);
        this.power = power;
        this.cores = cores;
        this.queue = new ArrayList<Job>();
        this.idleTime = 0;
        this.busyTime = 0;
        addInport("in");
        addOutport("out");

        //addTestInput("in", new Job("s1",1000000));
    }

    public void initialize() {
        phase = "passive";
        sigma = INFINITY;
        //phase = "busy";
        //sigma = 15;
        //this.queue.add(new Job("s1",1000000));
        //this.queue.add(new Job("s2",2000000));
        //this.queue.add(new Job("s3",3000000));
        super.initialize();
    }

    public void deltext(double e, message x) {
        Continue(e);
        Job job = (Job) getEntityOnPort(x, "in");
        if (phaseIs("passive")) {
            idleTime += e;
            currentJob = job;
            this.holdIn("busy", job.calcs / (this.cores * this.power));
        } else {
            busyTime += job.calcs / (this.cores * this.power);
            this.queue.add(job);
        }
    }

    /* Ειμαστε εντάξει με το currentJob αν καλείται πρώτα η out() και μετά η
       deltint() */
    public message out() {
        message m = new message();
        if (phaseIs("busy")) {
            m.add(makeContent("out", currentJob));
        }
        return m;
    }

    public void deltint() {
        if (phaseIs("busy")) {
            if (this.queue.size() > 0) {
                Job job = this.queue.get(0);
                this.queue.remove(0);
                this.holdIn("busy", job.calcs / (this.cores * this.power));
                currentJob = job;
            } else {
                passivate();
            }
        }
    }
}